USE [TravelAgency]
GO
/****** Object:  Table [dbo].[AdditionalServices]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdditionalServices](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BookId] [int] NULL,
	[ServiceId] [int] NULL,
 CONSTRAINT [PK_AdditionalServices] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[Patronymic] [varchar](255) NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FeedTypes]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeedTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
	[Price] [decimal](18, 5) NULL,
 CONSTRAINT [PK_FeedTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Requests]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Requests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[Patronymic] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
 CONSTRAINT [PK_Requests] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reviews]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reviews](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReviewText] [varchar](255) NULL,
	[ClientId] [int] NULL,
	[ServiceId] [int] NULL,
	[Stars] [int] NULL,
 CONSTRAINT [PK_Reviews] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Services]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Services](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
	[TypeId] [int] NULL,
	[Price] [decimal](18, 5) NULL,
 CONSTRAINT [PK_Services] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ServiceTypes]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
 CONSTRAINT [PK_ServiceTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TourBookings]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TourBookings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TourId] [int] NULL,
	[ClientId] [int] NULL,
	[FeedTypeId] [int] NULL,
	[TotalPrice] [decimal](18, 5) NULL,
	[WorkerId] [int] NULL,
	[HasVounchers] [bit] NULL,
 CONSTRAINT [PK_TourBookings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tours]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tours](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
	[StatedAt] [datetime] NULL,
	[EndedAt] [datetime] NULL,
	[ComfortLevel] [int] NULL,
	[Price] [decimal](18, 5) NULL,
 CONSTRAINT [PK_Tours] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TourServices]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TourServices](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TourId] [int] NULL,
	[ServiceId] [int] NULL,
 CONSTRAINT [PK_TourServices] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ToursToursTypes]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ToursToursTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [int] NULL,
	[TourId] [int] NULL,
 CONSTRAINT [PK_ToursToursTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TourTypes]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TourTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
 CONSTRAINT [PK_TourTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NULL,
	[UserId] [int] NULL,
	[Login] [varchar](255) NULL,
	[Password] [varchar](255) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Workers]    Script Date: 22.10.2023 23:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[Patronymic] [varchar](255) NULL,
	[RoleId] [int] NULL,
 CONSTRAINT [PK_Workers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AdditionalServices] ON 

INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1, NULL, 4)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (3, NULL, 7)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (4, NULL, 10)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (5, NULL, 7)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (7, NULL, 10)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (8, NULL, 8)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (9, NULL, 15)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (10, NULL, 15)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (11, NULL, 8)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (12, NULL, 15)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (15, 2013, 6)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (16, NULL, 2)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (18, NULL, 6)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (19, NULL, 10)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1008, NULL, 6)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1009, NULL, 15)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1013, NULL, 2)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1014, NULL, 3)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1015, NULL, 5)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1016, NULL, 7)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1021, NULL, 3)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1022, NULL, 10)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1023, NULL, 5)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1024, NULL, 14)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1025, NULL, 15)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1027, NULL, 4)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1028, NULL, 10)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1029, NULL, 7)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1030, NULL, 14)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1031, 3027, 3)
INSERT [dbo].[AdditionalServices] ([Id], [BookId], [ServiceId]) VALUES (1032, 3027, 9)
SET IDENTITY_INSERT [dbo].[AdditionalServices] OFF
GO
SET IDENTITY_INSERT [dbo].[Clients] ON 

INSERT [dbo].[Clients] ([Id], [FirstName], [LastName], [Patronymic]) VALUES (3, N'Наталья', N'Павлова', N'Сергеевна')
INSERT [dbo].[Clients] ([Id], [FirstName], [LastName], [Patronymic]) VALUES (4, N'Сергей', N'Михайлов', N'Игоревич')
INSERT [dbo].[Clients] ([Id], [FirstName], [LastName], [Patronymic]) VALUES (1004, N'Алексей', N'Марков', N'Евгеньевич')
INSERT [dbo].[Clients] ([Id], [FirstName], [LastName], [Patronymic]) VALUES (1005, N'gregre', N'gregre', N'gregre')
INSERT [dbo].[Clients] ([Id], [FirstName], [LastName], [Patronymic]) VALUES (1012, N'Антон', N'Анитонов', N'Антонович')
SET IDENTITY_INSERT [dbo].[Clients] OFF
GO
SET IDENTITY_INSERT [dbo].[FeedTypes] ON 

INSERT [dbo].[FeedTypes] ([Id], [Name], [Price]) VALUES (1, N'RO (Room Only)', CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[FeedTypes] ([Id], [Name], [Price]) VALUES (2, N'RR (Room Rate)', CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[FeedTypes] ([Id], [Name], [Price]) VALUES (3, N'OB (Only Bed)', CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[FeedTypes] ([Id], [Name], [Price]) VALUES (4, N'A (AccommodationOnly)', CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[FeedTypes] ([Id], [Name], [Price]) VALUES (5, N'BB (Bed Breakfast)', CAST(400.00000 AS Decimal(18, 5)))
INSERT [dbo].[FeedTypes] ([Id], [Name], [Price]) VALUES (6, N'HB (Half Board)', CAST(1000.00000 AS Decimal(18, 5)))
INSERT [dbo].[FeedTypes] ([Id], [Name], [Price]) VALUES (7, N'HB+ (Half Board Plus)', CAST(1600.00000 AS Decimal(18, 5)))
INSERT [dbo].[FeedTypes] ([Id], [Name], [Price]) VALUES (8, N'FB (Full Board)', CAST(2000.00000 AS Decimal(18, 5)))
INSERT [dbo].[FeedTypes] ([Id], [Name], [Price]) VALUES (9, N'FB+ (Full Board Plus)', CAST(3000.00000 AS Decimal(18, 5)))
INSERT [dbo].[FeedTypes] ([Id], [Name], [Price]) VALUES (10, N'AI (All Inclusive)', CAST(4000.00000 AS Decimal(18, 5)))
INSERT [dbo].[FeedTypes] ([Id], [Name], [Price]) VALUES (11, N'UAI (Ultra All Inclusive, ALL)', CAST(5000.00000 AS Decimal(18, 5)))
SET IDENTITY_INSERT [dbo].[FeedTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[Requests] ON 

INSERT [dbo].[Requests] ([Id], [FirstName], [LastName], [Patronymic], [Email]) VALUES (1, N'few', N'few', N'few', N'89581681323')
INSERT [dbo].[Requests] ([Id], [FirstName], [LastName], [Patronymic], [Email]) VALUES (3, N'Антон', N'Антонов', N'Антонович', N'sven557929@mail.ru')
SET IDENTITY_INSERT [dbo].[Requests] OFF
GO
SET IDENTITY_INSERT [dbo].[Reviews] ON 

INSERT [dbo].[Reviews] ([Id], [ReviewText], [ClientId], [ServiceId], [Stars]) VALUES (1002, N'sdsd', NULL, 2, 0)
INSERT [dbo].[Reviews] ([Id], [ReviewText], [ClientId], [ServiceId], [Stars]) VALUES (1003, N'Фигня какая то, было скучно', NULL, 2, 5)
INSERT [dbo].[Reviews] ([Id], [ReviewText], [ClientId], [ServiceId], [Stars]) VALUES (1004, N'gegerger', NULL, 2, 4)
INSERT [dbo].[Reviews] ([Id], [ReviewText], [ClientId], [ServiceId], [Stars]) VALUES (1006, N'Ткаой себе', 3, 2, 3)
SET IDENTITY_INSERT [dbo].[Reviews] OFF
GO
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([Id], [Name]) VALUES (1, N'Администратор')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (2, N'Менеджер')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (3, N'Клиент')
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO
SET IDENTITY_INSERT [dbo].[Services] ON 

INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (2, N'Отель "Eiffel Tower View"', 1, CAST(23000.00000 AS Decimal(18, 5)))
INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (3, N'Отель "Bali Luxury Resort"', 1, CAST(41900.00000 AS Decimal(18, 5)))
INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (4, N'Экскурсия по Парижу', 3, CAST(50000.00000 AS Decimal(18, 5)))
INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (5, N'Экскурсия в Лувр', 3, CAST(18000.00000 AS Decimal(18, 5)))
INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (6, N'Услуги гида-переводчика (полный день)', 3, CAST(5000.00000 AS Decimal(18, 5)))
INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (7, N'Оформление заграничного паспорта и визы (однократная)', 4, CAST(4700.00000 AS Decimal(18, 5)))
INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (8, N'Отель "Alps Ski Lodge"', 1, CAST(9200.00000 AS Decimal(18, 5)))
INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (9, N'Культурная экскурсия в Риме', 3, CAST(2100.00000 AS Decimal(18, 5)))
INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (10, N'Авиабилет из Москвы до Парижа', 2, CAST(48000.00000 AS Decimal(18, 5)))
INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (11, N'Авиабилет из Парижа до Москвы', 2, CAST(48000.00000 AS Decimal(18, 5)))
INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (12, N'Авиабилет из Москвы до Рима', 2, CAST(57000.00000 AS Decimal(18, 5)))
INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (13, N'Авиабилет из Рима до Москвы', 2, CAST(55000.00000 AS Decimal(18, 5)))
INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (14, N'Авиабилет из Москвы в Альпы', 2, CAST(34000.00000 AS Decimal(18, 5)))
INSERT [dbo].[Services] ([Id], [Name], [TypeId], [Price]) VALUES (15, N'Авиабилет из Альп до Москвы', 2, CAST(34000.00000 AS Decimal(18, 5)))
SET IDENTITY_INSERT [dbo].[Services] OFF
GO
SET IDENTITY_INSERT [dbo].[ServiceTypes] ON 

INSERT [dbo].[ServiceTypes] ([Id], [Name]) VALUES (1, N'Бронирование отелей и жилья')
INSERT [dbo].[ServiceTypes] ([Id], [Name]) VALUES (2, N'Транспортные услуги')
INSERT [dbo].[ServiceTypes] ([Id], [Name]) VALUES (3, N'Экскурсии и гиды')
INSERT [dbo].[ServiceTypes] ([Id], [Name]) VALUES (4, N'Услуги по оформлению виз и паспортов')
INSERT [dbo].[ServiceTypes] ([Id], [Name]) VALUES (5, N'Рестораны и питание')
INSERT [dbo].[ServiceTypes] ([Id], [Name]) VALUES (6, N'Развлечения и мероприятия')
INSERT [dbo].[ServiceTypes] ([Id], [Name]) VALUES (7, N'Другие туристические услуги')
SET IDENTITY_INSERT [dbo].[ServiceTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[TourBookings] ON 

INSERT [dbo].[TourBookings] ([Id], [TourId], [ClientId], [FeedTypeId], [TotalPrice], [WorkerId], [HasVounchers]) VALUES (2013, 3, 4, 3, CAST(91900.00000 AS Decimal(18, 5)), 2, 0)
INSERT [dbo].[TourBookings] ([Id], [TourId], [ClientId], [FeedTypeId], [TotalPrice], [WorkerId], [HasVounchers]) VALUES (3027, 2, 3, 1, CAST(67000.00000 AS Decimal(18, 5)), 2, NULL)
SET IDENTITY_INSERT [dbo].[TourBookings] OFF
GO
SET IDENTITY_INSERT [dbo].[Tours] ON 

INSERT [dbo].[Tours] ([Id], [Name], [StatedAt], [EndedAt], [ComfortLevel], [Price]) VALUES (2, N'Пляжный отдых на Балиfewfewfew', CAST(N'2023-04-24T00:00:00.000' AS DateTime), CAST(N'2023-04-26T00:00:00.000' AS DateTime), 5, CAST(23000.00000 AS Decimal(18, 5)))
INSERT [dbo].[Tours] ([Id], [Name], [StatedAt], [EndedAt], [ComfortLevel], [Price]) VALUES (3, N'Горнолыжный курорт в Альпах', CAST(N'2023-12-20T00:00:00.000' AS DateTime), CAST(N'2023-12-27T00:00:00.000' AS DateTime), 5, CAST(86900.00000 AS Decimal(18, 5)))
INSERT [dbo].[Tours] ([Id], [Name], [StatedAt], [EndedAt], [ComfortLevel], [Price]) VALUES (4, N'Экскурсия по Риму', CAST(N'2023-10-01T00:00:00.000' AS DateTime), CAST(N'2023-10-05T00:00:00.000' AS DateTime), 5, CAST(157800.00000 AS Decimal(18, 5)))
INSERT [dbo].[Tours] ([Id], [Name], [StatedAt], [EndedAt], [ComfortLevel], [Price]) VALUES (5, N'Лечебно-оздоровительный тур в Карпатах', CAST(N'2023-10-10T00:00:00.000' AS DateTime), CAST(N'2023-10-17T00:00:00.000' AS DateTime), 3, CAST(9700.00000 AS Decimal(18, 5)))
INSERT [dbo].[Tours] ([Id], [Name], [StatedAt], [EndedAt], [ComfortLevel], [Price]) VALUES (6, N'пкупукпупку', CAST(N'2024-04-24T00:00:00.000' AS DateTime), CAST(N'2024-04-24T00:00:00.000' AS DateTime), NULL, CAST(454.00000 AS Decimal(18, 5)))
INSERT [dbo].[Tours] ([Id], [Name], [StatedAt], [EndedAt], [ComfortLevel], [Price]) VALUES (10, N'ауцауц', CAST(N'2024-04-04T00:00:00.000' AS DateTime), CAST(N'2024-04-05T00:00:00.000' AS DateTime), NULL, CAST(43453.00000 AS Decimal(18, 5)))
INSERT [dbo].[Tours] ([Id], [Name], [StatedAt], [EndedAt], [ComfortLevel], [Price]) VALUES (11, N'fewfew', CAST(N'2024-04-24T00:00:00.000' AS DateTime), CAST(N'2024-04-24T00:00:00.000' AS DateTime), NULL, CAST(44.00000 AS Decimal(18, 5)))
INSERT [dbo].[Tours] ([Id], [Name], [StatedAt], [EndedAt], [ComfortLevel], [Price]) VALUES (16, N'gregre', CAST(N'2023-04-24T00:00:00.000' AS DateTime), CAST(N'2023-04-25T00:00:00.000' AS DateTime), 3, CAST(9200.00000 AS Decimal(18, 5)))
SET IDENTITY_INSERT [dbo].[Tours] OFF
GO
SET IDENTITY_INSERT [dbo].[TourServices] ON 

INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (1, NULL, 2)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (2, NULL, 4)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (3, NULL, 5)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (4, NULL, 6)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (5, NULL, 7)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (6, NULL, 10)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (7, NULL, 11)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (10, 3, 14)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (11, 3, 15)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (12, 3, 8)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (13, 3, 7)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (14, 3, 6)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (15, 4, 6)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (16, 4, 9)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (17, 4, 7)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (18, 4, 12)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (19, 4, 13)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (20, 5, 6)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (21, 5, 7)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (1014, NULL, 10)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (1015, NULL, 14)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (1017, NULL, 10)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (1020, NULL, 4)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (1022, NULL, 7)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (1023, NULL, 8)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (1027, NULL, 3)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (1030, NULL, 4)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (1031, NULL, 7)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (1033, NULL, 6)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (1034, NULL, 8)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (2033, 4, 15)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (2034, NULL, 2)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (2035, NULL, 4)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (2036, NULL, 11)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (2037, NULL, 15)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (2100, 2, 2)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (2103, NULL, 10)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (2104, NULL, 13)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (2107, NULL, 7)
INSERT [dbo].[TourServices] ([Id], [TourId], [ServiceId]) VALUES (2108, 16, 8)
SET IDENTITY_INSERT [dbo].[TourServices] OFF
GO
SET IDENTITY_INSERT [dbo].[ToursToursTypes] ON 

INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1, 1, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (2, 5, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (4, 1, 3)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (5, 7, 3)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (6, 1, 4)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (7, 8, 4)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (8, 5, 4)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (9, 1, 5)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (10, 4, 5)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1004, 2, 5)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1005, 4, 5)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1007, 7, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1010, 3, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1011, 9, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1012, 2, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1013, 4, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1014, 2, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1015, 6, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1016, 3, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1017, 9, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1018, 4, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1019, 1, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1020, 2, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1021, 8, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1022, 9, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1030, 2, 2)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1033, 2, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1034, 8, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1036, 3, NULL)
INSERT [dbo].[ToursToursTypes] ([Id], [TypeId], [TourId]) VALUES (1037, 3, 16)
SET IDENTITY_INSERT [dbo].[ToursToursTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[TourTypes] ON 

INSERT [dbo].[TourTypes] ([Id], [Name]) VALUES (1, N'Международный туризм')
INSERT [dbo].[TourTypes] ([Id], [Name]) VALUES (2, N'Внутренний туризм')
INSERT [dbo].[TourTypes] ([Id], [Name]) VALUES (3, N'Специализированные детские туры')
INSERT [dbo].[TourTypes] ([Id], [Name]) VALUES (4, N'Лечебно-оздоровительные туры')
INSERT [dbo].[TourTypes] ([Id], [Name]) VALUES (5, N'Экскурсионные туры')
INSERT [dbo].[TourTypes] ([Id], [Name]) VALUES (6, N'Обслуживание корпоративных клиентов по заказу')
INSERT [dbo].[TourTypes] ([Id], [Name]) VALUES (7, N'Горнолыжные курорты')
INSERT [dbo].[TourTypes] ([Id], [Name]) VALUES (8, N'Культурно-исторические туры')
INSERT [dbo].[TourTypes] ([Id], [Name]) VALUES (9, N'Пляжные туры')
SET IDENTITY_INSERT [dbo].[TourTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (1, 1, 1, N'admin', N'admin')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (2, 2, 2, N'man', N'1111')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (3, 2, 3, N'man1', N'1111')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (4, 2, 4, N'man2', N'1111')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (5, 2, 5, N'man3', N'1111')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (6, 3, 3, N'a', N'a')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (1004, 3, 1007, N'1', N'1')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (1005, 3, 3, N'pav', N'lova')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (1006, 3, 4, N'M4xl:w,x', N'9Lcw9fkc')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (2002, 3, 1008, N'sdsddsf', N'34356')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (3002, 3, 2008, N'hui', N'pizda')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (3003, 3, 1007, N's', N's')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (3004, 3, 1008, N'пкупку', N'пкупку')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (3005, 3, 1009, N'test', N'test1')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (3006, 3, 1010, N'fewfew', N'fewfewfew')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (3007, 3, 1011, N'gregre', N'gregre')
INSERT [dbo].[Users] ([Id], [RoleId], [UserId], [Login], [Password]) VALUES (3008, 3, 1012, N'fewfew', N'fewfew')
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
SET IDENTITY_INSERT [dbo].[Workers] ON 

INSERT [dbo].[Workers] ([Id], [FirstName], [LastName], [Patronymic], [RoleId]) VALUES (1, N'Иван', N'Иванов', N'Иванович', 1)
INSERT [dbo].[Workers] ([Id], [FirstName], [LastName], [Patronymic], [RoleId]) VALUES (2, N'Елена', N'Петрова', N'Сергеевна', 2)
INSERT [dbo].[Workers] ([Id], [FirstName], [LastName], [Patronymic], [RoleId]) VALUES (3, N'Алексей', N'Смирнов', N'Дмитриевич', 2)
INSERT [dbo].[Workers] ([Id], [FirstName], [LastName], [Patronymic], [RoleId]) VALUES (4, N'Мария', N'Козлова', N'Андреевна', 2)
INSERT [dbo].[Workers] ([Id], [FirstName], [LastName], [Patronymic], [RoleId]) VALUES (5, N'Петр', N'Федоров', N'Николаевич', 2)
SET IDENTITY_INSERT [dbo].[Workers] OFF
GO
ALTER TABLE [dbo].[AdditionalServices]  WITH CHECK ADD  CONSTRAINT [FK_AdditionalServices_Services] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Services] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[AdditionalServices] CHECK CONSTRAINT [FK_AdditionalServices_Services]
GO
ALTER TABLE [dbo].[AdditionalServices]  WITH CHECK ADD  CONSTRAINT [FK_AdditionalServices_TourBookings] FOREIGN KEY([BookId])
REFERENCES [dbo].[TourBookings] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AdditionalServices] CHECK CONSTRAINT [FK_AdditionalServices_TourBookings]
GO
ALTER TABLE [dbo].[Reviews]  WITH CHECK ADD  CONSTRAINT [FK_Reviews_Clients] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([Id])
GO
ALTER TABLE [dbo].[Reviews] CHECK CONSTRAINT [FK_Reviews_Clients]
GO
ALTER TABLE [dbo].[Reviews]  WITH CHECK ADD  CONSTRAINT [FK_Reviews_Services] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Services] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Reviews] CHECK CONSTRAINT [FK_Reviews_Services]
GO
ALTER TABLE [dbo].[Services]  WITH CHECK ADD  CONSTRAINT [FK_Services_ServiceTypes] FOREIGN KEY([TypeId])
REFERENCES [dbo].[ServiceTypes] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Services] CHECK CONSTRAINT [FK_Services_ServiceTypes]
GO
ALTER TABLE [dbo].[TourBookings]  WITH CHECK ADD  CONSTRAINT [FK_TourBookings_Clients] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[TourBookings] CHECK CONSTRAINT [FK_TourBookings_Clients]
GO
ALTER TABLE [dbo].[TourBookings]  WITH CHECK ADD  CONSTRAINT [FK_TourBookings_FeedTypes] FOREIGN KEY([FeedTypeId])
REFERENCES [dbo].[FeedTypes] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[TourBookings] CHECK CONSTRAINT [FK_TourBookings_FeedTypes]
GO
ALTER TABLE [dbo].[TourBookings]  WITH CHECK ADD  CONSTRAINT [FK_TourBookings_Tours] FOREIGN KEY([TourId])
REFERENCES [dbo].[Tours] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[TourBookings] CHECK CONSTRAINT [FK_TourBookings_Tours]
GO
ALTER TABLE [dbo].[TourBookings]  WITH CHECK ADD  CONSTRAINT [FK_TourBookings_Workers] FOREIGN KEY([WorkerId])
REFERENCES [dbo].[Workers] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[TourBookings] CHECK CONSTRAINT [FK_TourBookings_Workers]
GO
ALTER TABLE [dbo].[TourServices]  WITH CHECK ADD  CONSTRAINT [FK_TourServices_Services] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Services] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TourServices] CHECK CONSTRAINT [FK_TourServices_Services]
GO
ALTER TABLE [dbo].[TourServices]  WITH CHECK ADD  CONSTRAINT [FK_TourServices_Tours] FOREIGN KEY([TourId])
REFERENCES [dbo].[Tours] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TourServices] CHECK CONSTRAINT [FK_TourServices_Tours]
GO
ALTER TABLE [dbo].[ToursToursTypes]  WITH CHECK ADD  CONSTRAINT [FK_ToursToursTypes_Tours] FOREIGN KEY([TourId])
REFERENCES [dbo].[Tours] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ToursToursTypes] CHECK CONSTRAINT [FK_ToursToursTypes_Tours]
GO
ALTER TABLE [dbo].[ToursToursTypes]  WITH CHECK ADD  CONSTRAINT [FK_ToursToursTypes_TourTypes] FOREIGN KEY([TypeId])
REFERENCES [dbo].[TourTypes] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ToursToursTypes] CHECK CONSTRAINT [FK_ToursToursTypes_TourTypes]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Roles]
GO
ALTER TABLE [dbo].[Workers]  WITH CHECK ADD  CONSTRAINT [FK_Workers_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Workers] CHECK CONSTRAINT [FK_Workers_Roles]
GO
