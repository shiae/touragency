﻿using System.Collections.Generic;
using System.Linq;

namespace TourAgencyWPF
{
    public static class Mapper
    {
        public static List<TourViewModel> ToTourViewModelList(this List<Tours> tours)
        {
            return tours.Select(item => item.ToTourViewModel()).ToList();
        }

        public static TourViewModel ToTourViewModel(this Tours tour)
        {
            return new TourViewModel(tour);
        }

        public static Tours ToTour(this TourViewModel vm)
        {
            return vm.Tour;
        }

        public static List<ClientViewModel> ToClientViewModelList(this List<Clients> clients)
        {
            return clients.Select(item => item.ToClientViewModel()).ToList();
        }

        public static ClientViewModel ToClientViewModel(this Clients client)
        {
            return new ClientViewModel(client);
        }

        public static Clients ToClient(this ClientViewModel vm)
        {
            return vm.Client;
        }

        public static List<TourBookingViewModel> ToTourBookingViewModelList(this List<TourBookings> tourBookings)
        {
            return tourBookings.Select(item => item.ToTourBookingViewModel()).ToList();
        }

        public static TourBookingViewModel ToTourBookingViewModel(this TourBookings tourBooking)
        {
            return new TourBookingViewModel(tourBooking);
        }

        public static TourBookings ToTourBookings(this TourBookingViewModel viewModel)
        {
            return viewModel.TourBooking;
        }

        public static List<AdditionalServiceViewModel> ToAdditionalServiceViewModel(this List<AdditionalServices> additionalServices)
        {
            return additionalServices.Select(item => item.ToAdditionalServiceViewModel()).ToList();
        }

        public static AdditionalServiceViewModel ToAdditionalServiceViewModel(this AdditionalServices additionalService)
        {
            return new AdditionalServiceViewModel(additionalService);
        }

        public static AdditionalServices ToAdditionalService(this AdditionalServiceViewModel viewModel)
        {
            return viewModel.AdditionalService!;
        }

        public static List<RequestViewModel> ToRequestViewModelList(this List<Requests> requests)
        {
            return requests.Select(item => item.ToRequestViewModel()).ToList();
        }

        public static RequestViewModel ToRequestViewModel(this Requests request)
        {
            return new RequestViewModel(request);
        }

        public static Requests ToRequest(this RequestViewModel viewModel)
        {
            return viewModel.Request;
        }

        public static Reviews ToReview(this ReviewViewModel viewModel)
        {
            return viewModel.Review;
        }

        public static List<ReviewViewModel> ToReviewViewModelList(this List<Reviews> reviews)
        {
            return reviews.Select(item => item.ToReviewViewModel()).ToList();
        }

        public static ReviewViewModel ToReviewViewModel(this Reviews review)
        {
            return new ReviewViewModel(review);
        }
    }
}
