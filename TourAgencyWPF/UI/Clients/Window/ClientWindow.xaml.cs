﻿using System.Windows.Markup;

namespace TourAgencyWPF
{
    /// <summary>
    /// Логика взаимодействия для ClientWindow.xaml
    /// </summary>
    public partial class ClientWindow : BaseWindow<Clients>, IComponentConnector
    {

        #region Конструкторы

        public ClientWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Методы

        protected override void Open(Clients client, OpeningRegime openingRegime)
        {
            ClientWindowViewModel viewModel = new ClientWindowViewModel(client, openingRegime);
            this.DataContext = viewModel;
            this.ShowDialog();
        }

        #endregion
    }
}
