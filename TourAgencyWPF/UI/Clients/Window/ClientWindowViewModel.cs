﻿using Microsoft.IdentityModel.Tokens;
using ShirkunovISP31Cinema;
using System.Net.Mail;
using System.Net;
using System;
using System.Text;

namespace TourAgencyWPF
{
    public class ClientWindowViewModel : BaseWindowViewModel<Clients>
    {
        #region Публичные свойства

        public string? FirstName
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? LastName
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? Patronymic
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? Login
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? Password
        {
            get => Get<string>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        public ClientWindowViewModel(Clients entity, OpeningRegime openingRegime) : base(entity, openingRegime)
        {
            FirstName = entity.FirstName;
            LastName = entity.LastName;
            Patronymic = entity.Patronymic;
        }

        #endregion

        #region Методы BaseWindowViewModel

        public override void OnSave(object parameter)
        {
            if (SendDataToMail())
            {
                Clients client = new Clients();
                client.FirstName = FirstName;
                client.LastName = LastName;
                client.Patronymic = Patronymic;

                Save(client, parameter);
            }
        }

        public override string CheckBeforeSave()
        {
            StringBuilder error = new StringBuilder();

            if (FirstName.IsNullOrEmpty())
            {
                error.AppendLine("Введите имя");
            }

            if (LastName.IsNullOrEmpty())
            {
                error.AppendLine("Введите фамилию");
            }

            if (Patronymic.IsNullOrEmpty())
            {
                error.AppendLine("Введите отчество");
            }

            if (Login.IsNullOrEmpty())
            {
                error.AppendLine("Введите логин");
            }

            if (Password.IsNullOrEmpty())
            {
                error.AppendLine("Введите пароль");
            }

            return error.ToString();
        }

        private bool SendDataToMail()
        {
            string address = Microsoft.VisualBasic.Interaction.InputBox("Введите почту:");

            if (address.IsNullOrEmpty())
            {
                ShowErrorMessage("Была введена почта неправильного формата!");
                return false;
            }

            try
            {
                address = new MailAddress(address).Address;
            }
            catch (FormatException)
            {
                ShowErrorMessage("Была введена почта неправильного формата!");
                return false;
            }

            string smtpServer = "smtp.mail.ru";
            int smtpPort = 587;
            string smtpUsername = "sven557929@mail.ru";
            string smtpPassword = "7gptaSSzzpNt63fkUKSx";

            using (SmtpClient smtpClient = new SmtpClient(smtpServer, smtpPort))
            {
                smtpClient.Credentials = new NetworkCredential(smtpUsername, smtpPassword);
                smtpClient.EnableSsl = true;

                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress(smtpUsername);
                    mailMessage.To.Add(address);
                    mailMessage.Subject = "Данные от входа в ИС тур агенства";
                    mailMessage.Body = $"Логин: {Login} \r\nПароль: {Password}";

                    try
                    {
                        smtpClient.Send(mailMessage);
                        ShowMessage("Сообщение успешно отправлено");
                    }
                    catch (Exception ex)
                    {
                        ShowErrorMessage($"Ошибка отправки сообщения: {ex.Message}");
                    }
                }
            }

            return true;
        }

        #endregion
    }
}
