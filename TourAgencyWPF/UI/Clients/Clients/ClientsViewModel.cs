﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace TourAgencyWPF
{
    public class ClientsViewModel : BaseViewModel<ClientViewModel>
    {

        #region Публичные комманды

        public ICommand AddClientCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand MouseDoubleClickCommand { get; set; }

        #endregion

        #region Конструкторы

        public ClientsViewModel()
        {
            DeleteCommand = new RelayParameterizedCommand((object? parameter) => Delete(parameter));
            AddClientCommand = new RelayCommand(() => AddClient());
            Data = new ObservableCollection<ClientViewModel>(interactor.GetAllClients().ToClientViewModelList());
            MouseDoubleClickCommand = new RelayParameterizedCommand((object? parameter) => OpenClient(parameter));
        }

        #endregion

        #region Приватные методы

        private void Delete(object? parameter)
        {
            if (parameter != null && parameter is ClientViewModel vm)
            {
                MessageBoxResult result = MessageBox.Show(
                    "Вы уверены, что хотите удалить клиента?",
                    "Удаление тура",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Information
                );

                if (result == MessageBoxResult.Yes)
                {
                    interactor.DeleteClientById(vm.Id);
                    Data = new ObservableCollection<ClientViewModel>(interactor.GetAllClients().ToClientViewModelList());
                }
            }
        }

        private void AddClient()
        {
            ClientWindow window = new ClientWindow();
            window.OpenForAdd();

            if (window.DialogResult == true && window.DataContext != null && window.DataContext is ClientWindowViewModel viewModel)
            {
                Clients client = viewModel.OriginalEntity;
                Clients savedClient = interactor.SaveClient(client);
                Users user = new Users(3, savedClient.Id, viewModel.Login, viewModel.Password);
                interactor.SaveUser(user);
                AddData(savedClient.ToClientViewModel());
            }
        }

        private void OpenClient(object? parameter)
        {
            if (parameter != null && parameter is ClientViewModel vm)
            {
                ClientWindow window = new ClientWindow();
                window.OpenForView(vm.ToClient());
            }
        }

        #endregion
    }
}
