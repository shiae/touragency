﻿using System.Windows.Markup;

namespace TourAgencyWPF
{
    /// <summary>
    /// Логика взаимодействия для ClientsPage.xaml
    /// </summary>
    public partial class ClientsPage : BasePage<ClientsViewModel>, IComponentConnector
    {
        public ClientsPage()
        {
            InitializeComponent();
        }
    }
}
