﻿namespace TourAgencyWPF
{
    public class ClientViewModel : BaseViewModel
    {
        #region Публичные свойства

        public int Id
        {
            get => Get<int>();
            set => Set(value);
        }

        public string? FirstName
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? LastName
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? Patronymic
        {
            get => Get<string>();
            set => Set(value);
        }

        public Clients Client { get; set; }

        #endregion

        #region Конструкторы

        public ClientViewModel(Clients client)
        {
            Id = client.Id;
            FirstName = client.FirstName;
            LastName = client.LastName;
            Patronymic = client.Patronymic;
            Client = client;
        }

        #endregion
    }
}
