﻿using System.Text;
using System.Windows.Input;

namespace TourAgencyWPF
{
    public class LoginViewModel : BaseViewModel
    {
        #region Команды

        public ICommand LoginCommand { get; set; }
        public ICommand ForgotPasswordCommand { get; set; }
        public ICommand RequestCommand { get; set; }

        #endregion

        #region Публичные свойства

        public string? LoginProperty
        {
            get => Get<string?>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        public LoginViewModel()
        {
            LoginCommand = new RelayParameterizedCommand(parameter => Login(parameter));
            ForgotPasswordCommand = new RelayCommand(() => ForgotPassword());
            RequestCommand = new RelayCommand(() => OpenRequestPage());
        }

        #endregion

        #region Приватные методы

        private void Login(object? parameter)
        {
            if (parameter != null)
            {
                LoginPage? loginPage = parameter as LoginPage;

                if (!CheckErrorsBeforeLogin(loginPage))
                {
                    Users foundUser = interactor.GetUser(LoginProperty!, loginPage?.Password?.Password!);

                    ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
                    appVM.CurrentUser = foundUser;
                    appVM.GoToPage(new MainPage());
                }
            }
        }

        private void ForgotPassword()
        {
            ShowWarningMessage("Обратитесь для восстановления пароля к системному администратору!");
        }

        private bool CheckErrorsBeforeLogin(LoginPage? page)
        {
            StringBuilder errors = new StringBuilder();
            bool hasErrors = false;

            if (page != null)
            {
                if (string.IsNullOrEmpty(LoginProperty))
                {
                    TextBoxAttachedProperty.SetIsError(page.Login, true);
                    hasErrors = true;
                    errors.Append("Логин не может быть пустым").Append("\n");
                }

                if (string.IsNullOrEmpty(page.Password.Password))
                {
                    PasswordBoxAttachedProperty.SetIsError(page.Password, true);
                    hasErrors = true;
                    errors.Append("Пароль не может быть пустым").Append("\n");
                }

                if (errors.Length > 0 && hasErrors)
                {
                    ShowErrorMessage(errors.ToString());
                }
            }

            return hasErrors;
        }

        private void OpenRequestPage()
        {
            ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
            appVM.GoToPage(new RequestPage());
        }

        #endregion
    }
}
