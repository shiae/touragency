﻿namespace TourAgencyWPF
{
    public class AdditionalServiceViewModel : BaseViewModel
    {
        #region Публичные свойства

        public string? Id
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? Service
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? TypeOfService
        {
            get => Get<string>();
            set => Set(value);
        }

        public AdditionalServices? AdditionalService { get; set; }

        #endregion

        #region Конструкторы

        public AdditionalServiceViewModel(AdditionalServices additionalService)
        {
            Id = additionalService.Id.ToString();
            Service = additionalService?.Service?.Name;
            TypeOfService = additionalService?.Service?.Type?.Name;
            AdditionalService = additionalService;
        }

        #endregion
    }
}
