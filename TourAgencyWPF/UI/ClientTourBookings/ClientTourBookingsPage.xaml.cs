﻿using System.Windows.Markup;

namespace TourAgencyWPF
{
    /// <summary>
    /// Логика взаимодействия для ClientTourBookings.xaml
    /// </summary>
    public partial class ClientTourBookingsPage : BasePage<ClientTourBookingsViewModel>, IComponentConnector
    {
        public ClientTourBookingsPage()
        {
            InitializeComponent();
        }
    }
}
