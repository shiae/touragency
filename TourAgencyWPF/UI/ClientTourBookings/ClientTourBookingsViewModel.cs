﻿using System.Collections.ObjectModel;
using System.Windows.Input;

namespace TourAgencyWPF
{
    public class ClientTourBookingsViewModel : BaseViewModel<TourBookingViewModel>
    {
        #region Публичные свойства

        public ICommand MouseDoubleClickCommand { get; set; }

        #endregion

        #region Конструкторы

        public ClientTourBookingsViewModel()
        {
            ApplicationViewModel vm = IoC.Get<ApplicationViewModel>();
            Data = new ObservableCollection<TourBookingViewModel>(interactor.GetTourBookingsByClientId((int)vm.CurrentUser?.UserId!).ToTourBookingViewModelList());
            MouseDoubleClickCommand = new RelayParameterizedCommand((object? parameter) => Open(parameter));
        }

        #endregion

        #region Приватные методы

        private void Open(object? parameter)
        {
            if (parameter != null && parameter is TourBookingViewModel vm)
            {
                TourBookingWindow window = new TourBookingWindow();
                window.OpenForView(vm.ToTourBookings());
            }
        }

        #endregion
    }
}
