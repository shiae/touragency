﻿using System.Windows.Markup;

namespace TourAgencyWPF
{
    /// <summary>
    /// Логика взаимодействия для RequestWindow.xaml
    /// </summary>
    public partial class RequestWindow : BaseWindow<Requests>, IComponentConnector
    {
        public RequestWindow()
        {
            InitializeComponent();
        }

        protected override void Open(Requests entity, OpeningRegime openingRegime)
        {
            RequestWindowViewModel viewModel = new RequestWindowViewModel(entity, openingRegime);
            DataContext = viewModel;
            ShowDialog();
        }
    }
}
