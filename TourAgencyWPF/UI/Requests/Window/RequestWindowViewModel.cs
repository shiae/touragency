﻿using ShirkunovISP31Cinema;
using System;

namespace TourAgencyWPF
{
    public class RequestWindowViewModel : BaseWindowViewModel<Requests>
    {
        #region Публиные свойства

        public string? FirstName
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? LastName
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? Patronymic
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? Email
        {
            get => Get<string>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        public RequestWindowViewModel(Requests entity, OpeningRegime openingRegime) : base(entity, openingRegime)
        {
            FirstName = entity.FirstName;
            LastName = entity.LastName;
            Patronymic = entity.Patronymic;
            Email = entity.Email;
        }

        #endregion

        #region Методы RequestWindowViewModel

        public override string CheckBeforeSave()
        {
            throw new NotImplementedException();
        }

        public override void OnSave(object parameter)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
