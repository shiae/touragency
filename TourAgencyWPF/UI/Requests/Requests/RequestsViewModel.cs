﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace TourAgencyWPF
{
    public class RequestsViewModel : BaseViewModel<RequestViewModel>
    {
        #region Команды

        public ICommand MouseDoubleClickCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        #endregion

        #region Конструкторы

        public RequestsViewModel()
        {
            Data = new ObservableCollection<RequestViewModel>(interactor.GetRequests().ToRequestViewModelList());
            MouseDoubleClickCommand = new RelayParameterizedCommand((parameter) => Open(parameter));
            DeleteCommand = new RelayParameterizedCommand((parameter) => Delete(parameter));
        }

        #endregion

        #region Приватные методы

        private void Open(object? parameter)
        {
            if (parameter != null && parameter is RequestViewModel vm)
            {
                RequestWindow window = new RequestWindow();
                window.OpenForView(vm.ToRequest());
            }
        }

        private void Delete(object? parameter)
        {
            if (parameter != null && parameter is RequestViewModel vm)
            {
                MessageBoxResult result = MessageBox.Show(
                    "Вы уверены, что хотите удалить заявку?",
                    "Удаление заявки",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Information
                );

                if (result == MessageBoxResult.Yes)
                {
                    interactor.DeleteRequestById(vm.Id);
                    Data = new ObservableCollection<RequestViewModel>(interactor.GetRequests().ToRequestViewModelList());
                }
            }
        }

        #endregion
    }
}
