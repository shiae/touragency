﻿using System.Windows.Markup;

namespace TourAgencyWPF
{
    /// <summary>
    /// Логика взаимодействия для RequestsPage.xaml
    /// </summary>
    public partial class RequestsPage : BasePage<RequestsViewModel>, IComponentConnector
    {
        public RequestsPage()
        {
            InitializeComponent();
        }
    }
}
