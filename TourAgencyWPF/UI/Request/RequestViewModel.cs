﻿using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Windows.Input;

namespace TourAgencyWPF
{
    public class RequestViewModel : BaseViewModel
    {
        #region Команды

        public ICommand RequestCommand { get; set; }
        public ICommand LoginCommand { get; set; }

        #endregion

        #region Публиные свойства

        public int Id
        {
            get => Get<int>();
            set => Set(value);
        }

        public string? FirstNameProperty
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? LastNameProperty
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? PatronymicProperty
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? EmailProperty
        {
            get => Get<string>();
            set => Set(value);
        }

        public Requests Request { get; set; }

        #endregion

        #region Конструкторы

        public RequestViewModel(Requests request)
        {
            Id = request.Id;
            FirstNameProperty = request.FirstName;
            LastNameProperty = request.LastName;
            PatronymicProperty = request.Patronymic;
            EmailProperty = request.Email;
            Request = request;
            RequestCommand = new RelayParameterizedCommand((parameter) => SendRequest(parameter));
            LoginCommand = new RelayCommand(() => OpenLoginPage());
        }

        public RequestViewModel()
        {
            RequestCommand = new RelayParameterizedCommand((parameter) => SendRequest(parameter));
            LoginCommand = new RelayCommand(() => OpenLoginPage());
        }

        #endregion

        #region Приватные методы

        private void SendRequest(object? parameter)
        {
            if (parameter != null && parameter is RequestPage page)
            {
                string errors = CheckProperties(page);

                if (errors.IsNullOrEmpty())
                {
                    Requests request = new Requests(FirstNameProperty, LastNameProperty, PatronymicProperty, EmailProperty);
                    interactor.SaveRequests(request);
                    ShowMessage("Заявка успешно подана. Скоро с вами свяжется менеджер");
                }
                else
                {
                    ShowErrorMessage(errors);
                }
            }
        }

        private string CheckProperties(RequestPage page)
        {
            StringBuilder errors = new StringBuilder();

            if (FirstNameProperty.IsNullOrEmpty())
            {
                TextBoxAttachedProperty.SetIsError(page.FirstName, true);
                errors.AppendLine("Укажите имя!");
            }

            if (LastNameProperty.IsNullOrEmpty())
            {
                TextBoxAttachedProperty.SetIsError(page.LastName, true);
                errors.AppendLine("Укажите фамилию!");
            }

            if (PatronymicProperty.IsNullOrEmpty())
            {
                TextBoxAttachedProperty.SetIsError(page.Patronymic, true);
                errors.AppendLine("Укажите отчество!");
            }

            if (EmailProperty.IsNullOrEmpty())
            {
                TextBoxAttachedProperty.SetIsError(page.Email, true);
                errors.AppendLine("Укажите почту!");
            }

            return errors.ToString();
        }

        private void OpenLoginPage()
        {
            ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
            appVM.GoToPage(new LoginPage());
        }

        #endregion
    }
}
