﻿using System.Windows.Controls;
using System.Windows.Markup;

namespace TourAgencyWPF
{
    /// <summary>
    /// Логика взаимодействия для RequestPage.xaml
    /// </summary>
    public partial class RequestPage : BasePage<RequestViewModel>, IComponentConnector
    {
        public RequestPage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// При фокусе по элементу <see cref="TextBox"/> убирает ошибку
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TextBox_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            TextBox? element = sender as TextBox;

            if (element != null)
            {
                TextBoxAttachedProperty.SetIsError(element, false);
            }
        }
    }
}
