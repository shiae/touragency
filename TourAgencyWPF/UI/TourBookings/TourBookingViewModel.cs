﻿namespace TourAgencyWPF
{
    public class TourBookingViewModel : BaseViewModel
    {
        #region Публичные свойства

        public string? Id
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? TourId
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? ClientId
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? FeedTypeId
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? TotalPrice
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? WorkerId
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? Client
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? FeedType
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? Tour
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? Worker
        {
            get => Get<string>();
            set => Set(value);
        }

        public TourBookings TourBooking { get; set; }

        #endregion

        #region Конструкторы

        public TourBookingViewModel(TourBookings tourBooking)
        {
            Id = tourBooking.Id.ToString();
            TourId = tourBooking.Id.ToString();
            ClientId = tourBooking.ClientId.ToString();
            FeedTypeId = tourBooking.FeedTypeId.ToString();
            TotalPrice = tourBooking.TotalPrice.ToString();
            WorkerId = tourBooking.WorkerId.ToString();
            Client = tourBooking.Client == null ? null : tourBooking.Client.FIO;
            FeedType = tourBooking.FeedType?.Name;
            Tour = tourBooking.Tour?.Name;
            Worker = tourBooking.Worker == null ? null : tourBooking.Worker.FIO;
            TourBooking = tourBooking;
        }

        #endregion
    }
}
