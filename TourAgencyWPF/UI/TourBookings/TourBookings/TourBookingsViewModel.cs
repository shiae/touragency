﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace TourAgencyWPF
{
    public class TourBookingsViewModel : BaseViewModel<TourBookingViewModel>
    {

        #region Команды

        public ICommand DeleteCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand MouseDoubleClickCommand { get; set; }

        #endregion

        #region Конструкторы

        public TourBookingsViewModel()
        {
            Data = new ObservableCollection<TourBookingViewModel>(interactor.GetAllTourBookings().ToTourBookingViewModelList());
            DeleteCommand = new RelayParameterizedCommand((object? parameter) => Delete(parameter));
            AddCommand = new RelayCommand(() => Add());
            EditCommand = new RelayParameterizedCommand((object? parameter) => Edit(parameter));
            MouseDoubleClickCommand = new RelayParameterizedCommand((object? parameter) => Open(parameter));
        }

        #endregion

        #region Приватные методы

        private void Delete(object? parameter)
        {
            if (parameter != null && parameter is TourBookingViewModel vm)
            {
                MessageBoxResult result = MessageBox.Show(
                    "Вы уверены, что хотите удалить заявку?",
                    "Удаление заявки",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Information
                );

                if (result == MessageBoxResult.Yes)
                {
                    interactor.DeleteTourBookingById(vm.TourBooking.Id);
                    Data = new ObservableCollection<TourBookingViewModel>(interactor.GetAllTourBookings().ToTourBookingViewModelList());
                }
            }
        }

        private void Add()
        {
            TourBookingWindow window = new TourBookingWindow();
            window.OpenForAdd();

            if (window.DialogResult == true && window.DataContext != null && window.DataContext is TourBookingWindowViewModel viewModel)
            {
                TourBookings tourBookings = viewModel.OriginalEntity;
                interactor.SaveTourBooking(tourBookings);
                AddData(tourBookings.ToTourBookingViewModel());
            }
        }

        private void Open(object? parameter)
        {
            if (parameter != null && parameter is TourBookingViewModel vm)
            {
                TourBookingWindow window = new TourBookingWindow();
                window.OpenForView(vm.ToTourBookings());
            }
        }

        private void Edit(object? parameter)
        {
            if (parameter != null && parameter is TourBookingViewModel tourVM)
            {
                TourBookingWindow window = new TourBookingWindow();
                window.OpenForEdit(tourVM.ToTourBookings());

                if (window.DialogResult == true && window.DataContext != null && window.DataContext is TourBookingWindowViewModel vm)
                {
                    TourBookings newTour = vm.OriginalEntity;
                    interactor.DeleteAllAdditionalServices(newTour.Id);
                    interactor.Update(newTour);
                    Data = new ObservableCollection<TourBookingViewModel>(interactor.GetAllTourBookings().ToTourBookingViewModelList());
                }
            }
        }

        #endregion
    }
}
