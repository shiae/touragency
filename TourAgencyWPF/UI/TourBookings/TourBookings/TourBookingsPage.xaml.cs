﻿using System.Windows.Markup;

namespace TourAgencyWPF
{
    /// <summary>
    /// Логика взаимодействия для RequestsPage.xaml
    /// </summary>
    public partial class TourBookingsPage : BasePage<TourBookingsViewModel>, IComponentConnector
    {
        public TourBookingsPage()
        {
            InitializeComponent();
        }
    }
}
