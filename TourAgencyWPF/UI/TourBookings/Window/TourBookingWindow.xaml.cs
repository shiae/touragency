﻿using System.Windows.Markup;

namespace TourAgencyWPF
{
    /// <summary>
    /// Логика взаимодействия для TourBookingWindow.xaml
    /// </summary>
    public partial class TourBookingWindow : BaseWindow<TourBookings>, IComponentConnector
    {
        public TourBookingWindow()
        {
            InitializeComponent();
        }

        protected override void Open(TourBookings entity, OpeningRegime openingRegime)
        {
            TourBookingWindowViewModel viewModel = new TourBookingWindowViewModel(entity, openingRegime);
            this.DataContext = viewModel;
            this.ShowDialog();
        }
    }
}
