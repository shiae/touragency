﻿using Microsoft.IdentityModel.Tokens;
using Microsoft.Win32;
using ShirkunovISP31Cinema;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Windows;
using System.Windows.Input;
using iTextSharp.text.pdf;
using iTextSharp.text;
using Document = iTextSharp.text.Document;
using Font = iTextSharp.text.Font;

namespace TourAgencyWPF
{
    public class TourBookingWindowViewModel : BaseWindowViewModel<TourBookings>
    {
        #region Команды

        public ICommand DeleteCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand GetVaucherCommand { get; set; }

        #endregion

        #region Публичные свойства

        public string? Id
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? TotalPrice
        {
            get => Get<string>();
            set => Set(value);
        }

        public Clients? Client
        {
            get => Get<Clients>();
            set => Set(value);
        }

        public ObservableCollection<Clients>? Clients
        {
            get => Get<ObservableCollection<Clients>>();
            set => Set(value);
        }

        public FeedTypes? FeedType
        {
            get => Get<FeedTypes>();
            set
            {
                Set(value);
                RecalcTotalPrice();
            }
        }

        public ObservableCollection<FeedTypes>? FeedTypes
        {
            get => Get<ObservableCollection<FeedTypes>>();
            set => Set(value);
        }

        public Tours? Tour
        {
            get => Get<Tours>();
            set
            {
                Set(value);
                RecalcTotalPrice();
            }
        }

        public string? DateStart
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? DateEnd
        {
            get => Get<string>();
            set => Set(value);
        }

        public ObservableCollection<Tours>? Tours
        {
            get => Get<ObservableCollection<Tours>>();
            set => Set(value);
        }

        public Workers? Worker
        {
            get => Get<Workers>();
            set => Set(value);
        }

        public ObservableCollection<Workers>? Workers
        {
            get => Get<ObservableCollection<Workers>>();
            set => Set(value);
        }

        public ObservableCollection<AdditionalServices>? AdditionalServices
        {
            get => Get<ObservableCollection<AdditionalServices>>();
            set => Set(value);
        }

        public ObservableCollection<Services>? AllServices
        {
            get => Get<ObservableCollection<Services>>();
            set => Set(value);
        }

        public Services? SelectedService
        {
            get => Get<Services>();
            set => Set(value);
        }

        public bool AvailableVaucher
        {
            get => Get<bool>();
            set => Set(value);
        }

        public TourBookings TourBooking { get; set; }

        #endregion

        #region Конструкторы

        public TourBookingWindowViewModel(TourBookings tourBooking, OpeningRegime openingRegime) : base(tourBooking, openingRegime)
        {
            Users currentUser = IoC.Get<ApplicationViewModel>().CurrentUser!;
            TourBooking = tourBooking;
            Id = tourBooking.Id.ToString();
            TotalPrice = tourBooking.TotalPrice.ToString();
            Client = tourBooking.Client;
            FeedType = tourBooking.FeedType;
            Tour = tourBooking.Tour;
            Worker = openingRegime == OpeningRegime.ADD ? interactor.GetWorkerByUserId(currentUser.Id) : tourBooking.Worker;
            DateStart = Tour?.StatedAt?.ToString();
            DateEnd = Tour?.EndedAt?.ToString();
            AdditionalServices = new ObservableCollection<AdditionalServices>(tourBooking.AdditionalServices.ToList());
            Clients = new ObservableCollection<Clients>(interactor.GetAllClients());
            FeedTypes = new ObservableCollection<FeedTypes>(interactor.GetAllFeedTypes());
            Tours = new ObservableCollection<Tours>(interactor.GetTours());
            Workers = new ObservableCollection<Workers>(interactor.GetAllWorkers());
            AllServices = new ObservableCollection<Services>(interactor.GetServices());
            DeleteCommand = new RelayParameterizedCommand((parameter) => Delete(parameter));
            AddCommand = new RelayCommand(() => Add());
            GetVaucherCommand = new RelayCommand(() => GetVaucher());
            AvailableVaucher = currentUser.RoleId == 3;
        }

        #endregion

        #region Методы BaseWindowViewModel

        public override void OnSave(object parameter)
        {
            TourBookings tourBooking = new TourBookings();
            tourBooking.TotalPrice = decimal.Parse(TotalPrice!);
            tourBooking.Client = Client;
            tourBooking.FeedType = FeedType;
            tourBooking.Tour = Tour;
            tourBooking.Tour!.StatedAt = DateTime.Parse(DateStart!);
            tourBooking.Tour!.EndedAt = DateTime.Parse(DateEnd!);
            tourBooking.Worker = Worker;
            tourBooking.AdditionalServices = AdditionalServices?.ToList();

            Save(tourBooking, parameter);
        }

        public override string CheckBeforeSave()
        {
            StringBuilder errors = new StringBuilder();

            if (Tour == null)
            {
                errors.AppendLine("Необходимо выбрать тур!");
            }

            if (!DateTime.TryParse(DateStart, out DateTime dateStart))
            {
                errors.AppendLine("Необходимо указать дату начала правильного формата DD.MM.YYYY!");
            }

            if (!DateTime.TryParse(DateEnd, out DateTime dateEnd))
            {
                errors.AppendLine("Необходимо указать дату окончания правильного формата DD.MM.YYYY!");
            }

            if (!Decimal.TryParse(TotalPrice, out decimal totalPrice))
            {
                errors.AppendLine("Неверно указана стоимость!");
            }

            if (Client == null)
            {
                errors.AppendLine("Неверно указать клиента!");
            }

            if (FeedType == null)
            {
                errors.AppendLine("Неверно указать тип питания!");
            }

            return errors.ToString();
        }

        #endregion

        #region Приватные методы

        private void Add()
        {
            if (SelectedService != null)
            {
                if (AdditionalServices?.FirstOrDefault(item => item.ServiceId == SelectedService.Id) == null)
                {
                    AdditionalServices additionalServices = new AdditionalServices();
                    additionalServices.ServiceId = SelectedService.Id;
                    additionalServices.BookId = int.Parse(Id!); 
                    additionalServices.Service = SelectedService; 
                    additionalServices.Book = TourBooking; 
                    AdditionalServices?.Add(additionalServices);
                    SelectedService = null;
                    RecalcTotalPrice();
                }
                else
                {
                    ShowErrorMessage("Такая услуга уже есть в списке!");
                }
            }
        }

        private void Delete(object? parameter)
        {
            if (parameter != null && parameter is AdditionalServices additionalService)
            {
                AdditionalServices = new ObservableCollection<AdditionalServices>(AdditionalServices!.Where(item => item.ServiceId != additionalService.ServiceId).ToList());
                RecalcTotalPrice();
            }
        }

        private void RecalcTotalPrice()
        {
            decimal totalPrice = 0;

            if (FeedType != null)
            {
                totalPrice += FeedType.Price ?? 0;
            }

            if (Tour != null)
            {
                totalPrice += Tour.Price ?? 0;
            }

            if (AdditionalServices != null && !AdditionalServices.IsNullOrEmpty())
            {
                totalPrice += AdditionalServices.Sum(item => item?.Service?.Price ?? 0);
            }

            TotalPrice = totalPrice.ToString();
        }

        private void GetVaucher()
        {
            // Открываем диалог выбора места сохранения файла
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "PDF файлы (*.pdf)|*.pdf";
            saveFileDialog.Title = "Сохранить PDF файл";

            if (saveFileDialog.ShowDialog() == true)
            {
                string outputPath = saveFileDialog.FileName;

                // Создание документа
                Document doc = new Document();

                try
                {
                    // Создание файла для записи
                    FileStream fs = new FileStream(outputPath, FileMode.Create);
                    PdfWriter writer = PdfWriter.GetInstance(doc, fs);

                    // Открываем документ
                    doc.Open();

                    // Добавляем текст в документ
                    string hotelVoucher = $"Hotel voucher: Hotel XYZ\nName: Ivanov Ivan Ivanovich";
                    string visa = "Visa: Name: Ivanov Ivan Ivanovich";
                    string transferVoucher = "Voucher for transfer to the city: City ABC\nName: Ivanov Ivan Ivanovich";
                    string returnVoucher = "Return voucher: Place of departure: City ABC\nName: Ivanov Ivan Ivanovich";

                    // Шрифт для текста
                    BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    Font font = new Font(bf, 12, Font.NORMAL);

                    // Добавляем текст в документ
                    doc.Add(new Paragraph(hotelVoucher, font));
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(new Paragraph(visa, font));
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(new Paragraph(transferVoucher, font));
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(new Paragraph(returnVoucher, font));

                    // Закрываем документ
                    doc.Close();
                    fs.Close();

                    MessageBox.Show("PDF файл успешно создан и сохранен: " + outputPath);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Произошла ошибка при создании PDF файла: " + ex.Message);
                }
            }
        }

        #endregion
    }
}
