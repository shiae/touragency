﻿using System;

namespace TourAgencyWPF
{
    public class TourViewModel : BaseViewModel
    {

        #region Публичные свойства

        public int Id
        {
            get => Get<int>();
            set => Set(value);
        }

        public string? Name
        {
            get => Get<string>();
            set => Set(value);
        }

        public DateTime? StatedAt
        {
            get => Get<DateTime>();
            set => Set(value);
        }

        public DateTime? EndedAt
        {
            get => Get<DateTime>();
            set => Set(value);
        }

        public int? ComfortLevel
        {
            get => Get<int>();
            set => Set(value);
        }

        public decimal? Price
        {
            get => Get<decimal>();
            set => Set(value);
        }

        public Tours Tour { get; set; }

        #endregion

        #region Конструкторы

        public TourViewModel(Tours tour)
        {
            Id = tour.Id;
            Name = tour.Name;
            StatedAt = tour.StatedAt;
            EndedAt = tour.EndedAt;
            ComfortLevel = tour.ComfortLevel;
            Price = tour.Price;
            Tour = tour;
        }

        #endregion
    }
}
