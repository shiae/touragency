﻿using System.Windows.Markup;

namespace TourAgencyWPF
{
    /// <summary>
    /// Логика взаимодействия для TourWindow.xaml
    /// </summary>
    public partial class TourWindow : BaseWindow<Tours>, IComponentConnector
    {
        #region Конструкторы

        public TourWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Методы

        protected override void Open(Tours tour, OpeningRegime openingRegime)
        {
            TourWindowViewModel viewModel = new TourWindowViewModel(tour, openingRegime);
            this.DataContext = viewModel;
            this.ShowDialog();
        }

        #endregion
    }
}
