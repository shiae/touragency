﻿using Microsoft.IdentityModel.Tokens;
using ShirkunovISP31Cinema;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace TourAgencyWPF
{
    public class TourWindowViewModel : BaseWindowViewModel<Tours>
    {
        #region Команды

        public ICommand AddTourTypeCommand { get; set; }
        public ICommand DeleteTourTypeCommand { get; set; }
        public ICommand AddServiceCommand { get; set; }
        public ICommand DeleteServiceCommand { get; set; }

        #endregion

        #region Свойства

        public string? Name
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? StartedAt
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? EndedAt
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? Price
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? ComfortLevel
        {
            get => Get<string>();
            set => Set(value);
        }

        public ObservableCollection<TourTypes>? TourTypes
        {
            get => Get<ObservableCollection<TourTypes>>();
            set => Set(value);
        }

        public ObservableCollection<TourTypes>? AllTourTypes
        {
            get => Get<ObservableCollection<TourTypes>>();
            set => Set(value);
        }

        public ObservableCollection<Services>? Services
        {
            get => Get<ObservableCollection<Services>>();
            set => Set(value);
        }

        public ObservableCollection<Services>? AllServices
        {
            get => Get<ObservableCollection<Services>>();
            set => Set(value);
        }

        public TourTypes? SelectedTourType
        {
            get => Get<TourTypes>();
            set => Set(value);
        }

        public Services? SelectedService
        {
            get => Get<Services>();
            set => Set(value);
        }

        #endregion

        #region Конструктор

        public TourWindowViewModel(Tours entity, OpeningRegime openingRegime) : base(entity, openingRegime)
        {
            Name = entity.Name;
            StartedAt = entity.StatedAt?.ToString();
            EndedAt = entity.EndedAt?.ToString();
            Price = entity.Price?.ToString();
            ComfortLevel = entity.ComfortLevel.ToString();

            if (entity.ToursToursTypes != null)
            {
                TourTypes = new ObservableCollection<TourTypes>(entity.ToursToursTypes.Select(item => item.Type).ToList());
            }

            AllTourTypes = new ObservableCollection<TourTypes>(interactor.GetAllTourTypes());

            if (entity.TourServices != null)
            {
                Services = new ObservableCollection<Services>(entity.TourServices.Select(item => item.Service).ToList());
            }

            AllServices = new ObservableCollection<Services>(interactor.GetServices());

            AddTourTypeCommand = new RelayCommand(() => AddTourType());
            DeleteTourTypeCommand = new RelayParameterizedCommand((parameter) => DeleteTourType(parameter));
            AddServiceCommand = new RelayCommand(() => AddService());
            DeleteServiceCommand = new RelayParameterizedCommand((parameter) => DeleteService(parameter));
        }

        #endregion

        #region Приватные методы

        private void AddTourType()
        {
            if (SelectedTourType != null)
            {
                if (TourTypes?.Where(item => item.Id == SelectedTourType.Id).FirstOrDefault() == null)
                {
                    TourTypes?.Add(SelectedTourType);
                    SelectedTourType = null;
                    RecalcTotalPrice();
                }
                else
                {
                    ShowErrorMessage("Такой тип уже есть в списке!");
                }
            }
        }

        private void DeleteTourType(object? parameter)
        {
            if (parameter != null && parameter is TourTypes tourType)
            {
                TourTypes = new ObservableCollection<TourTypes>(TourTypes!.Where(item => item.Id != tourType.Id).ToList());
                RecalcTotalPrice();
            }
        }

        private void AddService()
        {
            if (SelectedService != null)
            {
                if (Services?.Where(item => item.Id == SelectedService.Id).FirstOrDefault() == null)
                {
                    Services?.Add(SelectedService);
                    SelectedService = null;
                    RecalcTotalPrice();
                }
                else
                {
                    ShowErrorMessage("Такая услуга уже есть в списке!");
                }
            }
        }

        private void DeleteService(object? parameter)
        {
            if (parameter != null && parameter is Services services)
            {
                Services = new ObservableCollection<Services>(Services!.Where(item => item.Id != services.Id).ToList());
                RecalcTotalPrice();
            }
        }

        private void RecalcTotalPrice()
        {
            if (!Services.IsNullOrEmpty())
            {
                Price = Services?.Sum(item => item.Price).ToString();
            }
            else
            {
                Price = "0";
            }
        }

        #endregion

        #region Методы TourWindowViewModel

        public override void OnSave(object parameter)
        {
            Tours tour = new Tours();
            tour.Name = Name;
            tour.StatedAt = DateTime.Parse(StartedAt!);
            tour.EndedAt = DateTime.Parse(EndedAt!);
            tour.Price = Decimal.Parse(Price!);
            tour.ComfortLevel = int.Parse(ComfortLevel!);
            tour.TourServices = Services?.Select(service =>
            {
                TourServices tourServices = new TourServices();
                tourServices.Service = service;
                tourServices.Tour = tour;
                return tourServices;
            }).ToList();
            tour.ToursToursTypes = TourTypes?.Select(type =>
            {
                ToursToursTypes tourTypes = new ToursToursTypes();
                tourTypes.Type = type;
                tourTypes.Tour = tour;
                return tourTypes;
            }).ToList();

            Save(tour, parameter);
        }

        public override string CheckBeforeSave()
        {
            StringBuilder error = new StringBuilder();

            if (!DateTime.TryParse(StartedAt, out DateTime convertedStartDate))
            {
                error.AppendLine("Неверно введена дата начала. Введите дату формата DD.MM.YYYY");
            }

            if (!DateTime.TryParse(EndedAt, out DateTime convertedEndDate))
            {
                error.AppendLine("Неверно введена дата окончания. Введите дату формата DD.MM.YYYY");
            }

            if (!Decimal.TryParse(Price, out decimal convertedPrice))
            {
                error.AppendLine("Введена некорректная стоимость");
            }

            if (Name.IsNullOrEmpty())
            {
                error.AppendLine("Введите наименование");
            }

            if (ComfortLevel.IsNullOrEmpty())
            {
                error.AppendLine("Введите уровень комфорта");
            }

            if (int.TryParse(ComfortLevel, out int parsed))
            {
                if (parsed < 0 || parsed > 5)
                {
                    error.AppendLine("Уровень комфорта должен быть от 0 до 5");
                }
            }
            else
            {
                error.AppendLine("Введен некорректный уровень комфорта");
            }

            if (TourTypes.IsNullOrEmpty())
            {
                error.AppendLine("Добавьте типы туров!");
            }

            if (Services.IsNullOrEmpty())
            {
                error.AppendLine("Добавьте услуги!");
            }

            return error.ToString();
        }

        #endregion
    }
}
