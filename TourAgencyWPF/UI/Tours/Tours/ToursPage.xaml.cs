﻿using System.Windows.Markup;

namespace TourAgencyWPF
{
    /// <summary>
    /// Логика взаимодействия для ToursPage.xaml
    /// </summary>
    public partial class ToursPage : BasePage<ToursViewModel>, IComponentConnector
    {
        public ToursPage()
        {
            InitializeComponent();
        }
    }
}
