﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace TourAgencyWPF
{
    public class ToursWithAdditionalViewModel : BaseViewModel<TourViewModel>
    {
        #region Приватные свойства

        private Timer? filterTimer;

        #endregion

        #region Публичные комманды

        public ICommand FilterByNameCommand { get; set; }
        public ICommand FilterByPriceCommand { get; set; }
        public ICommand FilterByPriceLessCommand { get; set; }
        public ICommand AddTourCommand { get; set; }
        public ICommand MouseDoubleClickCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand FilterByDateStartCommand { get; set; }
        public ICommand FilterByDateEndCommand { get; set; }

        #endregion

        #region Публичные свойства

        public string? FilterNameProperty
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? FilterPriceProperty
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? FilterPriceLessProperty
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? FilterDateStartProperty
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? FilterDateEndProperty
        {
            get => Get<string>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        public ToursWithAdditionalViewModel()
        {
            Data = new ObservableCollection<TourViewModel>(interactor.GetTours().ToTourViewModelList());
            FilterByNameCommand = new RelayParameterizedCommand((object? parameter) => FilterByProperty(parameter, (value) => FilterNameProperty = value));
            FilterByPriceCommand = new RelayParameterizedCommand((object? parameter) => FilterByProperty(parameter, (value) => FilterPriceProperty = value));
            FilterByPriceLessCommand = new RelayParameterizedCommand((object? parameter) => FilterByProperty(parameter, (value) => FilterPriceLessProperty = value));
            FilterByDateStartCommand = new RelayParameterizedCommand((object? parameter) => FilterByProperty(parameter, (value) => FilterDateStartProperty = value));
            FilterByDateEndCommand = new RelayParameterizedCommand((object? parameter) => FilterByProperty(parameter, (value) => FilterDateEndProperty = value));
            MouseDoubleClickCommand = new RelayParameterizedCommand((object? parameter) => OpenTour(parameter));
            AddTourCommand = new RelayCommand(() => AddTour());
            EditCommand = new RelayParameterizedCommand((object? parameter) => Edit(parameter));
            DeleteCommand = new RelayParameterizedCommand((object? parameter) => Delete(parameter));
        }

        #endregion

        #region Приватные методы

        private void FilterByProperty(object? parameter, Action<string?> setProperty)
        {
            if (parameter != null)
            {
                setProperty(parameter.ToString());
            }
            else
            {
                setProperty(null);
            }

            FilterTours();
        }

        private void FilterTours()
        {
            filterTimer?.Dispose();
            filterTimer = new Timer(async (object? state) =>
            {
                Predicate<object>? predicate = (item) =>
                {
                    if (item is TourViewModel tour)
                    {
                        bool result = true;

                        if (!FilterNameProperty.IsNullOrEmpty())
                        {
                            result = tour.Name!.ToLower().Contains(FilterNameProperty!.ToLower());
                        }

                        if (Decimal.TryParse(FilterPriceProperty, out decimal inputPrice))
                        {
                            result = result && tour.Price > inputPrice;
                        }

                        if (Decimal.TryParse(FilterPriceLessProperty, out decimal inputPriceLess))
                        {
                            result = result && tour.Price < inputPriceLess;
                        }

                        if (DateTime.TryParse(FilterDateStartProperty, out DateTime inputDateStart))
                        {
                            result = result && tour.StatedAt >= inputDateStart;
                        }

                        if (DateTime.TryParse(FilterDateEndProperty, out DateTime inputDateEnd))
                        {
                            result = result && tour.EndedAt <= inputDateEnd;
                        }

                        return result;
                    }
                    else
                    {
                        return false;
                    }
                };

                await Application.Current.Dispatcher.Invoke(() =>
                {
                    DataCollectionView.Filter = predicate;
                    DataCollectionView.Refresh();
                    CountOfRows = DataCollectionView.Cast<object>().Count().ToString();
                    return Task.CompletedTask;
                });

            }, null, 500, Timeout.Infinite);
        }

        private void AddTour()
        {
            TourWindow window = new TourWindow();
            window.OpenForAdd();

            if (window.DialogResult == true && window.DataContext != null && window.DataContext is TourWindowViewModel viewModel)
            {
                Tours newTour = viewModel.OriginalEntity;
                Tours savedTour = interactor.SaveTour(newTour);
                AddData(savedTour.ToTourViewModel());
            }
        }

        private void OpenTour(object? parameter)
        {
            if (parameter != null && parameter is TourViewModel tourVM)
            {
                TourWindow window = new TourWindow();
                window.OpenForView(tourVM.ToTour());
            }
        }

        private void Edit(object? parameter)
        {
            if (parameter != null && parameter is TourViewModel tourVM)
            {
                TourWindow window = new TourWindow();
                window.OpenForEdit(tourVM.ToTour());

                if (window.DialogResult == true && window.DataContext != null && window.DataContext is TourWindowViewModel vm)
                {
                    Tours newTour = vm.OriginalEntity;
                    interactor.DeleteAllToursToursTypes(newTour.Id);
                    interactor.DeleteAllTourServices(newTour.Id);
                    interactor.Update(newTour);
                    Data = new ObservableCollection<TourViewModel>(interactor.GetTours().ToTourViewModelList());
                }
            }
        }

        private void Delete(object? parameter)
        {
            if (parameter != null && parameter is TourViewModel tourVM)
            {
                MessageBoxResult result = MessageBox.Show(
                    "Вы уверены, что хотите удалить тур?",
                    "Удаление тура",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Information
                );

                if (result == MessageBoxResult.Yes)
                {
                    interactor.DeleteTourById(tourVM.Id);
                    Data = new ObservableCollection<TourViewModel>(interactor.GetTours().ToTourViewModelList());
                }
            }
        }

        #endregion
    }
}
