﻿using System.Windows.Markup;

namespace TourAgencyWPF
{
    /// <summary>
    /// Логика взаимодействия для ToursWithAdditional.xaml
    /// </summary>
    public partial class ToursWithAdditionalPage : BasePage<ToursWithAdditionalViewModel>, IComponentConnector
    {
        public ToursWithAdditionalPage()
        {
            InitializeComponent();
        }
    }
}
