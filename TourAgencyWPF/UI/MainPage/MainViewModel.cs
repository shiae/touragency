﻿using System;
using System.Windows;
using System.Windows.Input;

namespace TourAgencyWPF
{
    public class MainViewModel : BaseViewModel
    {
        #region Команды

        public ICommand LoadedCommand { get; set; }
        public ICommand ExitFromAccountCommand { get; set; }

        #endregion

        #region Публичные свойства

        public ToursPage? TourPage
        {
            get => Get<ToursPage>();
            set => Set(value);
        }

        public ToursWithAdditionalPage? ToursWithAdditionalPage
        {
            get => Get<ToursWithAdditionalPage>();
            set => Set(value);
        }

        public ClientsPage? ClientsPage
        {
            get => Get<ClientsPage>();
            set => Set(value);
        }

        public TourBookingsPage? TourBookingsPage
        {
            get => Get<TourBookingsPage>();
            set => Set(value);
        }

        public RequestsPage? RequestsPage
        {
            get => Get<RequestsPage>();
            set => Set(value);
        }

        public ClientTourBookingsPage? ClientTourBookingsPage
        {
            get => Get<ClientTourBookingsPage>();
            set => Set(value);
        }

        public ReviewsPage? ReviewsPage
        {
            get => Get<ReviewsPage>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        public MainViewModel()
        {
            TourPage = new ToursPage();
            ToursWithAdditionalPage = new ToursWithAdditionalPage();
            ClientsPage = new ClientsPage();
            TourBookingsPage = new TourBookingsPage();
            RequestsPage = new RequestsPage();
            ClientTourBookingsPage = new ClientTourBookingsPage();
            ReviewsPage = new ReviewsPage();
            LoadedCommand = new RelayParameterizedCommand((parameter) => OnLoaded(parameter));
            ExitFromAccountCommand = new RelayCommand(() => ExitFromAccount());
        }

        #endregion

        #region Приватные методы

        private void OnLoaded(object? sender)
        {
            if (sender != null && (sender.GetType() == typeof(MainPage)))
            {
                MainPage? mainPage = (MainPage)sender;

                try
                {
                    ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
                    Users user = appVM.CurrentUser!;

                    // Администратор
                    if (user.RoleId == 1)
                    {
                        mainPage.Tours.Visibility = Visibility.Collapsed;
                        mainPage.ToursWithAdditional.Visibility = Visibility.Visible;
                        mainPage.Requests.Visibility = Visibility.Collapsed;
                        mainPage.Clients.Visibility = Visibility.Collapsed;
                        mainPage.AccountGroup.Visibility = Visibility.Collapsed;
                        mainPage.PlaneCar.Visibility = Visibility.Collapsed;
                        mainPage.Star.Visibility = Visibility.Collapsed;
                        mainPage.tabControl.SelectedItem = mainPage.ToursWithAdditional;
                    }
                    // Менеджер
                    else if (user.RoleId == 2)
                    {
                        mainPage.Tours.Visibility = Visibility.Visible;
                        mainPage.ToursWithAdditional.Visibility = Visibility.Collapsed;
                        mainPage.Requests.Visibility = Visibility.Visible;
                        mainPage.Clients.Visibility = Visibility.Visible;
                        mainPage.AccountGroup.Visibility = Visibility.Visible;
                        mainPage.PlaneCar.Visibility = Visibility.Collapsed;
                        mainPage.Star.Visibility = Visibility.Collapsed;
                        mainPage.tabControl.SelectedItem = mainPage.Tours;
                    }
                    // Клиент
                    else
                    {
                        mainPage.Tours.Visibility = Visibility.Visible;
                        mainPage.ToursWithAdditional.Visibility = Visibility.Collapsed;
                        mainPage.Requests.Visibility = Visibility.Collapsed;
                        mainPage.Clients.Visibility = Visibility.Collapsed;
                        mainPage.AccountGroup.Visibility = Visibility.Collapsed;
                        mainPage.PlaneCar.Visibility = Visibility.Visible;
                        mainPage.Star.Visibility = Visibility.Visible;
                        mainPage.tabControl.SelectedItem = mainPage.Tours;
                    }
                }
                catch (ApplicationException exception)
                {
                    ShowErrorMessage(exception.Message);
                }
            }
        }

        private void ExitFromAccount()
        {
            MessageBoxResult result = MessageBox.Show("Вы точно хотите выйти из аккаунта?", "Выход из аккаунта", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {
                ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
                appVM.CurrentUser = null;
                appVM.GoToPage(new LoginPage());
            }
        }

        #endregion
    }
}
