﻿using System.Windows.Markup;

namespace TourAgencyWPF
{
    /// <summary>
    /// Логика взаимодействия для MainPage.xaml
    /// </summary>
    public partial class MainPage : BasePage<MainViewModel>, IComponentConnector
    {
        public MainPage()
        {
            InitializeComponent();
        }
    }
}
