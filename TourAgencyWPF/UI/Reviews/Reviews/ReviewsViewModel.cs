﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace TourAgencyWPF
{
    public class ReviewsViewModel : BaseViewModel<ReviewViewModel>
    {
        #region Команды

        public ICommand MouseDoubleClickCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand AddReviewCommand { get; set; }

        #endregion

        #region Конструкторы

        public ReviewsViewModel()
        {
            MouseDoubleClickCommand = new RelayParameterizedCommand((parameter) => Open(parameter));
            DeleteCommand = new RelayParameterizedCommand((parameter) => Delete(parameter));
            AddReviewCommand = new RelayCommand(() => Add());
            GetReviews();
        }

        #endregion

        #region Приватные методы

        private void Open(object? parameter)
        {
            if (parameter != null && parameter is ReviewViewModel vm)
            {
                ReviewWindow window = new ReviewWindow();
                window.OpenForView(vm.ToReview());
            }
        }

        private void Delete(object? parameter)
        {
            if (parameter != null && parameter is ReviewViewModel vm)
            {
                MessageBoxResult result = MessageBox.Show(
                    "Вы уверены, что хотите удалить отзыв?",
                    "Удаление отзыва",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Information
                );

                if (result == MessageBoxResult.Yes)
                {
                    interactor.DeleteReviewById(vm.Id);
                    GetReviews();
                }
            }
        }

        private void Add()
        {
            ReviewWindow window = new ReviewWindow();
            window.OpenForAdd();

            if (window.DialogResult == true && window.DataContext != null && window.DataContext is ReviewWindowViewModel viewModel)
            {
                Reviews newReview = viewModel.OriginalEntity;
                Reviews savedReview = interactor.SaveReview(newReview);
                AddData(savedReview.ToReviewViewModel());
            }
        }

        private void GetReviews()
        {
            ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
            Data = new ObservableCollection<ReviewViewModel>(interactor.GetReviewsByClientId((int)appVM.CurrentUser?.UserId!).ToReviewViewModelList());
        }

        #endregion
    }
}
