﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TourAgencyWPF
{
    public class ReviewViewModel : BaseViewModel
    {
        #region Публичные свойства

        public int Id
        {
            get => Get<int>();
            set => Set(value);
        }

        public string? ServiceName
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? ReviewText
        {
            get => Get<string>();
            set => Set(value);
        }

        public int? Stars
        {
            get => Get<int>();
            set => Set(value);
        }

        public Reviews Review { get; set; }

        #endregion

        #region Конструкторы

        public ReviewViewModel(Reviews review)
        {
            Id = review.Id;
            ServiceName = review.Service?.Name;
            ReviewText = review.ReviewText;
            Stars = review.Stars;
            Review = review;
        }

        #endregion
    }
}
