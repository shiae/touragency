﻿using Microsoft.IdentityModel.Tokens;
using ShirkunovISP31Cinema;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TourAgencyWPF
{
    public class ReviewWindowViewModel : BaseWindowViewModel<Reviews>
    {
        #region Публичные свойства

        public ObservableCollection<Services>? Services
        {
            get => Get<ObservableCollection<Services>>();
            set => Set(value);
        }

        public Services? SelectedService
        {
            get => Get<Services>();
            set => Set(value);
        }

        public string? Stars
        {
            get => Get<string>();
            set => Set(value);
        }

        public string? ReviewText
        {
            get => Get<string>();
            set => Set(value);
        }

        #endregion

        public ReviewWindowViewModel(Reviews entity, OpeningRegime openingRegime) : base(entity, openingRegime)
        {
            Stars = entity.Stars.ToString();
            ReviewText = entity.ReviewText;
            SelectedService = entity.Service;
            ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
            Services = new ObservableCollection<Services>(interactor.GetServicesForReview((int)appVM.CurrentUser?.UserId!));

            if (SelectedService != null)
            {
                Services.Add(SelectedService);
            }
        }

        public override void OnSave(object parameter)
        {
            ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
            Reviews review = new Reviews();
            review.Stars = int.Parse(Stars!);
            review.ReviewText = ReviewText;
            review.Service = SelectedService;
            review.ClientId = (int)appVM.CurrentUser?.UserId!;

            Save(review, parameter);
        }

        public override string CheckBeforeSave()
        {
            StringBuilder errors = new StringBuilder();

            if (int.TryParse(Stars, out int parsed))
            {
                if (parsed < 0 || parsed > 10)
                {
                    errors.AppendLine("Оценка должны быть от 0 до 10");
                }
            }
            else
            {
                errors.AppendLine("Некорректная оценка была введена!");
            }

            if (ReviewText.IsNullOrEmpty())
            {
                errors.AppendLine("Напишите отзыв!");
            }

            if (SelectedService == null)
            {
                errors.AppendLine("Выберите услугу!");
            }

            return errors.ToString();
        }
    }
}