﻿using System.Windows.Controls;

namespace TourAgencyWPF
{
    public abstract class BasePage : UserControl
    {
        #region Публичные свойства

        /// <summary>
        /// ViewModel конкретной страницы
        /// </summary>
        public object? ViewModel { get; set; }

        #endregion
    }

    public class BasePage<VM> : BasePage
        where VM : BaseViewModel, new()
    {
        #region Конструктор

        public BasePage()
        {
            ViewModel = new VM();
            DataContext = ViewModel;
        }

        #endregion
    }
}