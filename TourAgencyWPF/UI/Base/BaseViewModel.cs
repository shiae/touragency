﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Data;

namespace TourAgencyWPF
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        private readonly Dictionary<string, object> _propertyValues = new Dictionary<string, object>();

        protected Interactor interactor => IoC.Get<Interactor>();

        public event PropertyChangedEventHandler? PropertyChanged = (sender, e) => { };

        protected T? Get<T>([CallerMemberName] string propertyName = null!)
        {
            if (_propertyValues.TryGetValue(propertyName, out object? value))
            {
                return (T)value;
            }

            return default(T);
        }

        protected void Set<T>(T value, [CallerMemberName] string propertyName = null!)
        {
            if (!EqualityComparer<T>.Default.Equals(Get<T>(propertyName), value))
            {
                _propertyValues[propertyName] = value;
                OnPropertyChanged(propertyName);
            }
        }

        public void OnPropertyChanged([CallerMemberName] string? name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void ShowErrorMessage(string message)
        {
            MessageBox.Show(
                message,
                "Ошибка",
                MessageBoxButton.OK,
                MessageBoxImage.Error
           );
        }

        public void ShowWarningMessage(string message)
        {
            MessageBox.Show(
                message,
                "Предупреждение",
                MessageBoxButton.OK,
                MessageBoxImage.Information
           );
        }

        public void ShowMessage(string message)
        {
            MessageBox.Show(
                message,
                "Уведомление",
                MessageBoxButton.OK,
                MessageBoxImage.Information
           );
        }
    }

    public abstract class BaseViewModel<Entity> : BaseViewModel
        where Entity : class
    {

        public ObservableCollection<Entity> Data
        {
            get => Get<ObservableCollection<Entity>>()!;
            set
            {
                Set(value);
                CountOfRows = value.Count.ToString();
                DataCollectionView = CollectionViewSource.GetDefaultView(value);
                DataCollectionView.SortDescriptions.Add(new SortDescription("Id", ListSortDirection.Ascending));
            }
        }

        public ICollectionView DataCollectionView
        {
            get => Get<ICollectionView>()!;
            set => Set(value);
        }

        public string CountOfRows
        {
            get => Get<string>()!;
            set => Set($"{value} строк");
        }

        public void AddData(Entity entity)
        {
            Data.Add(entity);
            CountOfRows = DataCollectionView.Cast<object>().Count().ToString();
        }
    }
}
