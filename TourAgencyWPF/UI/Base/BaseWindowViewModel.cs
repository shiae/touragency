﻿using Microsoft.IdentityModel.Tokens;
using System.Windows;
using System.Windows.Input;
using TourAgencyWPF;

namespace ShirkunovISP31Cinema
{
    public abstract class BaseWindowViewModel<Entity> : BaseViewModel
        where Entity : BaseEntity
    {
        #region Свойства

        public Entity OriginalEntity
        {
            get => Get<Entity>();
            set => Set(value);
        }

        public OpeningRegime OpeningRegime
        {
            get => Get<OpeningRegime>();
            set => Set(value);
        }

        public bool EditedField
        {
            get => Get<bool>();
            set => Set(value);
        }

        #endregion

        #region Команды

        public ICommand CloseWindowCommand { get; set; }

        public ICommand SaveWindowCommand { get; set; }

        #endregion

        #region Конструкторы

        public BaseWindowViewModel(Entity entity, OpeningRegime openingRegime)
        {
            OpeningRegime = openingRegime;
            OriginalEntity = entity;
            CloseWindowCommand = new RelayParameterizedCommand((object parameter) => OnCloseWindow(parameter));
            SaveWindowCommand = new RelayParameterizedCommand((object parameter) => OnSaveWithCheks(parameter));
        }

        #endregion

        #region Методы

        public void OnCloseWindow(object parameter)
        {
            if (parameter != null && parameter is Window window)
            {
                if (EditedField)
                {
                    MessageBoxResult result = MessageBox.Show(
                        "Есть несохраненные данные, вы уверены, что хотите закрыть? Изменения не будут сохранены",
                        "Сохранение данных",
                        MessageBoxButton.YesNo,
                        MessageBoxImage.Information
                    );

                    if (result == MessageBoxResult.No)
                    {
                        return;
                    }
                }

                window.DialogResult = false;
                window.Close();
            }
        }

        public void Save(Entity entity, object parameter)
        {
            if (parameter != null && parameter is Window window)
            {
                MessageBoxResult result = MessageBox.Show(
                    "Вы уверены, что хотите сохранить данные?",
                    "Сохранение данных",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Information
                );

                if (result == MessageBoxResult.Yes)
                {
                    if (OpeningRegime == OpeningRegime.EDIT)
                    {
                        entity.Id = OriginalEntity.Id;
                    }

                    OriginalEntity = entity;
                    EditedField = false;
                    window.DialogResult = true;
                }
            }
        }

        private void OnSaveWithCheks(object parameter)
        {
            string message = CheckBeforeSave();

            if (message.IsNullOrEmpty())
            {
                OnSave(parameter);
                return;
            }

            ShowErrorMessage(message);
        }

        #endregion

        #region Абстрактные методы

        public abstract void OnSave(object parameter);

        public abstract string CheckBeforeSave();

        #endregion
    }
}
