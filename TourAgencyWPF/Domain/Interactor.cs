﻿using Microsoft.IdentityModel.Tokens;
using Ninject.Infrastructure.Language;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TourAgencyWPF
{
    /// <summary>
    /// Интерактор, который взаимодействует с несколькими репозиториями
    /// </summary>
    public class Interactor
    {

        #region Приватные свойства

        private BaseRepository<Users> _usersRepository;
        private BaseRepository<Tours> _toursRepository;
        private BaseRepository<Clients> _clientRepository;
        private BaseRepository<TourBookings> _tourBookingsRepository;
        private BaseRepository<FeedTypes> _feedTypesRepository;
        private BaseRepository<Workers> _workersRepository;
        private BaseRepository<AdditionalServices> _additionalServiceRepository;
        private BaseRepository<Services> _servicesRepository;
        private BaseRepository<ServiceTypes> _typeRepository;
        private BaseRepository<Requests> _requestRepository;
        private BaseRepository<TourTypes> _tourTypesRepository;
        private BaseRepository<TourServices> _tourServicesRepository;
        private BaseRepository<ToursToursTypes> _toursToursTypesRepository;
        private BaseRepository<Reviews> _reviewRepository;

        #endregion

        #region Конструкторы

        public Interactor()
        {
            _usersRepository = new BaseRepository<Users>();
            _toursRepository = new BaseRepository<Tours>();
            _clientRepository = new BaseRepository<Clients>();
            _tourBookingsRepository = new BaseRepository<TourBookings>();
            _feedTypesRepository = new BaseRepository<FeedTypes>();
            _workersRepository = new BaseRepository<Workers>();
            _additionalServiceRepository = new BaseRepository<AdditionalServices>();
            _servicesRepository = new BaseRepository<Services>();
            _typeRepository = new BaseRepository<ServiceTypes>();
            _requestRepository = new BaseRepository<Requests>();
            _tourTypesRepository = new BaseRepository<TourTypes>();
            _tourServicesRepository = new BaseRepository<TourServices>();
            _toursToursTypesRepository = new BaseRepository<ToursToursTypes>();
            _reviewRepository = new BaseRepository<Reviews>();
        }

        #endregion

        #region Публичные методы

        public Users GetUser(string login, string password)
        {
            Users? foundUser = _usersRepository.FindAll().FirstOrDefault(item => item.Login == login && item.Password == password);

            if (foundUser != null)
            {
                return foundUser;
            }

            throw new TourAgencyException("Пользователь с такими данными не найден!");
        }

        public List<Tours> GetTours()
        {
            return _toursRepository.FindAll().Select(item =>
            {
                item.TourBookings = GetAllTourBookings().Where(foundTB => foundTB.TourId == item.Id).ToList();
                item.TourServices = GetAllTourServices().Where(foundS => foundS.TourId == item.Id).ToList();
                item.ToursToursTypes = GetToursToursTypes().Where(foundT => foundT.TourId == item.Id).ToList();
                return item;
            }).ToList();
        }

        public Tours SaveTour(Tours tour)
        {
            return _toursRepository.Insert(tour);
        }

        public void Update(Tours tour)
        {
            _toursRepository.Update(tour);
        }

        public void Update(TourBookings tour)
        {
            _tourBookingsRepository.Update(tour);
        }

        public void DeleteTourById(int id)
        {
            _toursRepository.Delete(id);
        }

        public void DeleteClientById(int id)
        {
            _clientRepository.Delete(id);
        }

        public List<Clients> GetAllClients()
        {
            return _clientRepository.FindAll();
        }

        public Clients SaveClient(Clients client)
        {
            return _clientRepository.Insert(client);
        }

        public Clients? GetClientById(int id)
        {
            return _clientRepository.GetById(id);
        }

        public Users SaveUser(Users user)
        {
            return _usersRepository.Insert(user);
        }

        public List<TourBookings> GetAllTourBookings()
        {
            return _tourBookingsRepository.FindAll().Select(item =>
            {
                if (item.WorkerId != null)
                {
                    item.Worker = _workersRepository.GetById((int)item.WorkerId);
                }

                item.AdditionalServices = GetAdditionalServices().Where(addService => addService.BookId == item.Id).ToList();
                return item;
            }).ToList();
        }

        public TourBookings SaveTourBooking(TourBookings tourBooking)
        {
            return _tourBookingsRepository.Insert(tourBooking);
        }

        public void DeleteTourBookingById(int id)
        {
            _tourBookingsRepository.Delete(id);
        }

        public TourBookings? GetTourBookingById(int id)
        {
            return _tourBookingsRepository.GetById(id);
        }

        public List<TourBookings> GetTourBookingsByClientId(int clientId)
        {
            return _tourBookingsRepository.FindAll().Where(item => item.ClientId == clientId).Select(item =>
            {
                if (item.WorkerId != null)
                {
                    item.Worker = _workersRepository.GetById((int)item.WorkerId);
                }

                return item;
            }).ToList();
        }

        public List<FeedTypes> GetAllFeedTypes()
        {
            return _feedTypesRepository.FindAll();
        }

        public List<Workers> GetAllWorkers()
        {
            return _workersRepository.FindAll();
        }

        public List<AdditionalServices> GetAdditionalServices()
        {
            return _additionalServiceRepository.FindAll().Select(item =>
            {
                if (item.ServiceId != null)
                {
                    item.Service = _servicesRepository.GetById((int)item.ServiceId);

                    if (item.Service != null)
                    {
                        item.Service.Type = _typeRepository.GetById((int)item.Service.TypeId!);
                    }
                }

                if (item.BookId != null)
                {
                    item.Book = _tourBookingsRepository.GetById((int)item.BookId);
                }

                return item;
            }).ToList();
        }

        public Workers? GetWorkerByUserId(int userId)
        {
            return _workersRepository.GetById(userId);
        }

        public Requests SaveRequests(Requests requests)
        {
            return _requestRepository.Insert(requests);
        }

        public List<Requests> GetRequests()
        {
            return _requestRepository.FindAll();
        }

        public void DeleteRequestById(int id)
        {
            _requestRepository.Delete(id);
        }

        public List<TourTypes> GetAllTourTypes()
        {
            return _tourTypesRepository.FindAll();
        }

        public List<Services> GetServices()
        {
            return _servicesRepository.FindAll();
        }

        public List<TourServices> GetAllTourServices()
        {
            return _tourServicesRepository.FindAll().Select(item =>
            {
                item.Service = GetServices().Where(service => service.Id == item.ServiceId).FirstOrDefault();
                return item;
            }).ToList();
        }

        public List<ToursToursTypes> GetToursToursTypes()
        {
            return _toursToursTypesRepository.FindAll().Select(item =>
            {
                item.Type = GetAllTourTypes().Where(tour => tour.Id == item.TypeId).FirstOrDefault();
                return item;
            }).ToList();
        }

        public void DeleteReviewById(int id)
        {
            _reviewRepository.Delete(id);
        }

        public List<Reviews> GetReviewsByClientId(int clientId)
        {
            return _reviewRepository.FindAll().Where(item => item.ClientId == clientId).Select(item =>
            {
                item.Service = GetServices().Where(service => service.Id == item.ServiceId).FirstOrDefault();
                return item;
            }).ToList();
        }

        public List<Services> GetServicesForReview(int clientId)
        {
            List<TourBookings> tourBookings = GetTourBookingsByClientId(clientId);
            List<Tours?> tours = tourBookings.Select(tourBooking => GetTours().Where(tour => DateTime.Now >= tour.EndedAt && tour.Id == tourBooking.TourId).FirstOrDefault()).ToList();
            
            if (tours.IsNullOrEmpty())
            {
                return new List<Services>();
            }
            else
            {
                return tours.Select(tour => GetAllTourServices()
                        .Where(tourService => (tourService.TourId ?? 0) == tour?.Id)
                        .Select(tourService => GetServices().Where(service => service.Id == tourService.ServiceId).First())
                        .ToList())
                    .SelectMany(item => item)
                    .ToList();
            }
        }

        public Reviews SaveReview(Reviews review)
        {
            return _reviewRepository.Insert(review);
        }

        public void DeleteAllToursToursTypes(int tourId)
        {
            _toursToursTypesRepository.FindAll().Where(item => item.TourId == tourId).ToList().ForEach(item => _toursToursTypesRepository.Delete(item.Id));
        }

        public void DeleteAllTourServices(int tourId)
        {
            _tourServicesRepository.FindAll().Where(item => item.TourId == tourId).ToList().ForEach(item => _tourServicesRepository.Delete(item.Id));
        }

        public void DeleteAllAdditionalServices(int tourBookingId)
        {
            _additionalServiceRepository.FindAll().Where(item => item.BookId == tourBookingId).ToList().ForEach(item => _additionalServiceRepository.Delete(item.Id));
        }

        #endregion
    }
}
