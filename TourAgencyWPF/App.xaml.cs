﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace TourAgencyWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Метод для настройки глобальных настроек приложения
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // Настраиваем главное окно приложения
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            DispatcherUnhandledException += App_DispatcherUnhandledException;
            TaskScheduler.UnobservedTaskException += OnUnobservedTaskException!;

            // Настраиваем и показываем MainWindow
            Current.MainWindow = new MainWindow();
            MainWindowViewModel mainWindowViewModel = MainWindowViewModel.Create(Current.MainWindow);
            Current.MainWindow.DataContext = mainWindowViewModel;
            Current.MainWindow.Show();

            // Настраиваем ApplicationViewModel
            ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
            appVM.CurrentPagePropertyChanged += (s, e) => mainWindowViewModel.OnPropertyChanged(nameof(mainWindowViewModel.CurrentPage));
            appVM.GoToPage(new LoginPage());
        }

        /// <summary>
        /// Используется в качестве глобального обработчика событий
        /// </summary>
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ShowGlobalErrorMessage(((Exception)e.ExceptionObject));
        }

        /// <summary>
        /// Используется в качестве глобального обработчика событий
        /// </summary>
        private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            ShowGlobalErrorMessage(e.Exception);
        }

        /// <summary>
        /// Используется когда происходит ошибка в асинхронном коде
        /// </summary>
        private void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            e.SetObserved();
            ShowGlobalErrorMessage(e.Exception);
        }

        /// <summary>
        /// Используется для показа глобального сообщения
        /// </summary>
        /// <param name="message"></param>
        private void ShowGlobalErrorMessage(Exception e)
        {
            if (e is TourAgencyException)
            {
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                MessageBox.Show($"Не волнуйтесь! Произошла непредвиденная ошибка приложения: {e.Message}. Обратитесь к системному администратору!", "Непредвиденная ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
