﻿using System;
using System.Globalization;

namespace TourAgencyWPF
{
    public class OpeningRegimeToBooleanReverseConverter : BaseValueConverter<OpeningRegimeToBooleanReverseConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            OpeningRegime openingRegime = (OpeningRegime)value;
            return openingRegime != OpeningRegime.VIEW;
        }
    }
}
