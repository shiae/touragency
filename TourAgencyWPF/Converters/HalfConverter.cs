﻿using System;
using System.Globalization;

namespace TourAgencyWPF
{
    public class HalfConverter : BaseValueConverter<HalfConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double size)
            {
                return size / 2;
            }

            return value;
        }
    }
}
