﻿using System;

namespace TourAgencyWPF
{
    public class TourAgencyException : Exception
    {
        public TourAgencyException(string? message = null) : base(message)
        {

        }
    }
}
