﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace TourAgencyWPF
{
    public class BaseRepository<Entity>
        where Entity : BaseEntity
    {
        private TravelAgencyContext context => IoC.Get<TravelAgencyContext>();

        private readonly Microsoft.EntityFrameworkCore.DbSet<Entity> _set;

        public BaseRepository()
        {
            _set = context.Set<Entity>();
        }

        public List<Entity> FindAll()
        {
            return _set.ToList();
        }

        public List<Entity> Find(Expression<Func<Entity, bool>> predicate)
        {
            return _set.Where(predicate).ToList();
        }

        public Entity? GetById(int id)
        {
            return _set.Find(id);
        }

        public Entity Insert(Entity entity)
        {
            _set.Add(entity);
            context.SaveChanges();
            return entity;
        }

        public void Update(Entity entity)
        {
            var trackedEntity = context.Set<Entity>().SingleOrDefault(e => e.Id == entity.Id);
            context.Entry(trackedEntity).State = EntityState.Detached;
            context.Set<Entity>().Attach(entity);
            context.Entry<Entity>(entity).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            Entity? entityToDelete = GetById(id);
            if (entityToDelete != null)
            {
                context.SaveChanges();
                _set.Remove(entityToDelete);
                context.SaveChanges();
            }
        }
    }
}
