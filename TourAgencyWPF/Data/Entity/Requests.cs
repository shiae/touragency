﻿namespace TourAgencyWPF
{
    public partial class Requests : BaseEntity
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Patronymic { get; set; }
        public string? Email { get; set; }

        public Requests(int id, string? firstName, string? lastName, string? patronymic, string? email)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Patronymic = patronymic;
            Email = email;
        }

        public Requests(string? firstName, string? lastName, string? patronymic, string? email)
        {
            FirstName = firstName;
            LastName = lastName;
            Patronymic = patronymic;
            Email = email;
        }

        public Requests()
        {

        }
    }
}
