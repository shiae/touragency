﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace TourAgencyWPF
{
    public partial class Roles : BaseEntity
    {
        public Roles()
        {
            Users = new HashSet<Users>();
            Workers = new HashSet<Workers>();
        }

        public string Name { get; set; }

        public virtual ICollection<Users> Users { get; set; }
        public virtual ICollection<Workers> Workers { get; set; }
    }
}