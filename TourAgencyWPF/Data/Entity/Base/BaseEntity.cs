﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TourAgencyWPF
{
    /// <summary>
    /// Базовый класс для определения сущностей в приложении
    /// </summary>
    public abstract class BaseEntity
    {
        #region Свойства

        /// <summary>
        /// ID сущности
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        #endregion
    }
}
