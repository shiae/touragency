﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace TourAgencyWPF
{
    public partial class TourServices : BaseEntity
    {
        public int? TourId { get; set; }
        public int? ServiceId { get; set; }

        public virtual Services Service { get; set; }
        public virtual Tours Tour { get; set; }
    }
}